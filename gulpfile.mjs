/**
 * Gulpfile
 *
 * @author Takuto Yanagida
 * @version 2024-04-19
 */

const SUB_REPS_SASS = [
	'align',
	'container',
	'content',
	'font',
	'form',
	'ja',
	'link',
	'list',
	'reset',
	'tab',
	'table',
	'utility',
	'viewer',
];

const SUB_REPS_JS = [
	'align',
	'container',
	'content',
	'delay',
	'ja',
	'link',
	'list',
	'scroll',
	'tab',
	'table',
	'utility',
	'viewer',
];

import gulp from 'gulp';

import { makeCopyTask } from './gulp/task-copy.mjs';

export const update = async done => {
	const { pkgDir }       = await import('./gulp/common.mjs');
	const { makeSassTask } = await import('./gulp/task-sass.mjs');
	SUB_REPS_SASS.map(e => makeCopyTask(`${pkgDir(`nacss-${e}`)}/src/sass/*`, `./src/sass/${e}/`)());
	SUB_REPS_JS.map(e => makeCopyTask(`${pkgDir(`nacss-${e}`)}/src/js/*`, `./src/js/${e}/`)());
	makeSassTask('src/sass/reset/*.scss', './src/css', './src/sass/reset')();
	done();
};
export default gulp.parallel(makeCopyTask('src/**/*', './dist/'));
